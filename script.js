$(function () {
  $('#scotch-panel').scotchPanel({
    containerSelector: 'body',
    direction: 'right',
    duration: 300,
    transition: 'ease',
    clickSelector: '.toggle-panel',
    distanceX: '80%',
    enableEscapeKey: true
  })
})

// Toggle close & burger - imperfect because you need to click directly on the icon and not just the toggle area (could get stuck on either one)
function togglePanelIcon(x) {
  x.classList.toggle("fa-times")
}

let footer = document.getElementById('footerLinks')

// move footer, nav, searchbar when at TAB-PORT size
window.addEventListener("resize", function (event) {
  if (document.body.clientWidth < 900) {
    document.getElementById('navbar').appendChild(document.getElementById('registerTag'))
    // document.getElementById('scotch-footer').appendChild(document.getElementById('footerLinks'))
    // if (footer.classList.contains("col")) {
    //   document.getElementById('footerLinks').classList.remove("col")
    // }
    // document.getElementById('scotch-nav').appendChild(document.getElementById('navigation'))

  } else {
    document.getElementById('registerSpace').appendChild(document.getElementById('registerTag'))
    // document.getElementById('footerSpace').appendChild(document.getElementById('footerLinks'))
    // if (footer.classList.contains("col") == false) {
    //   document.getElementById('footerLinks').classList.add("col")
    // }
    // document.getElementById('navSpace').appendChild(document.getElementById('navigation'))
  }
})

// This should enable the div to move more reliably when window loads on different mobile devices/orientation changes
if (window.innerWidth < 900) {
  document.getElementById('navbar').appendChild(document.getElementById('registerTag'))
  // document.getElementById('scotch-footer').appendChild(document.getElementById('footerLinks'))
  // document.getElementById('scotch-nav').appendChild(document.getElementById('navbar'))
  // document.getElementById('scotch-nav').appendChild(document.getElementById('searchForm'))
  // if (footer.classList.contains("col")) 
  //     document.getElementById('footerLinks').classList.remove("col")
  // }
  // document.getElementById('scotch-nav').appendChild(document.getElementById('navigation'))
} else {
  document.getElementById('registerSpace').appendChild(document.getElementById('registerTag'))
  // document.getElementById('footerSpace').appendChild(document.getElementById('footerLinks'))
  // if (footer.classList.contains("col") == false) {
  //     document.getElementById('footerLinks').classList.add("col")
  // }
  // document.getElementById('navSpace').appendChild(document.getElementById('navigation'))
}