## 🚨 UPDATED : Zeroseven Assessment

:star2: **Please refer to this version** :star2:
> **https://zero7.netlify.com/** & **https://bitbucket.org/rachel-wong/07/src/master/** 

This original version can still be referred to (especially for dev notes). However there will be improvements made in the updated version. 
> https://zeroseven.netlify.com/

![Desktop size](/assets/fullsize.gif)

### Outstanding issues & What isn't implemented

* Bootstrap grid config is off. resulting in alot of visual guestimation :eyes:.

* VanillaJS script to move `<div>` on resizing window is flakey. User has to reload the screen at full-size first before resizing downwards before they can access the content inside the scotch panel (pull out side panel). **The current workaround is to duplicate the nav and footer content in the scotch panel, which is not an ideal solution.**

Below is the JS script tested to be working in sandbox.

```javascript
    window.addEventListener("resize", function (event) {
      if (document.body.clientWidth < 768) {
        document.getElementById('header').appendChild(document.getElementById('child'))
        console.log("move links to header")
      } else {
        document.getElementById('footer').appendChild(document.getElementById('child'))
        console.log('move links back to footer')
      }
    })
```

* ~~As a result of the flakey JS script, the **Register** section does not reliably appear in the header section when the window resizes to tablet-portrait and lower. This happens especially when the browser is refreshed at those sizes.~~ EDIT: This should be fixed by checking for `window.innerWidth` instead.

* On the offcanvas scotch-panel menu at mobile screen sizes, user need to click **SPECIFICALLY** on the burger and cross icons to open/close the panel. Otherwise, this will lead to incorrect icons to represent different states.

* In the panic to fix broken things, some commits could be broken up into smaller compartmentalisd pushes. :angry:

* Unable to resize the search bar in the pull-out offcanvas (scotch-panel) slider.

* `nav` does not degrade responsively between screensizes.

* No responsive images

* Checked the banner on mobile sizes, it seems the carousel image is stuck when flipping between portrait and landscape orientation.

* Unable to activate the drop shadow on the scotch panel.

* Unsure whether there is liberty to change text sizes responsively

### 🧠 What I learnt

> For all experiments, please see the `/sandbox` directory.

* SVG's added inline as <img> can have their colour changed by inputting the hex values into [this tool](https://codepen.io/sosuke/pen/Pjoqqp) and added as `css`
* watching for screen width changes is [possible](https://stackoverflow.com/questions/2172800/automatically-detect-web-browser-window-width-change) using vanillaJS
* moving a div into another div can also be [done](https://stackoverflow.com/questions/6329108/moving-a-div-from-inside-one-div-to-another-div-using-prototype) via vanillaJS.
* [Tried](https://stackoverflow.com/questions/641857/javascript-window-resize-event) the `onresize` event but did not find it especially helpful, but will pin for future reference just in case.
* Managed to use [scotch panel](https://panels.scotch.io/) to replicate a version of the responsive toggled sidebar menu, rather than hard-code own version.
* For the register buttons - how to position an `absolute` element in the middle [stackoverflow](https://stackoverflow.com/questions/16758102/how-do-i-horizontally-center-an-absolute-positioned-element-inside-a-100-width)
* Found the `window.innerWidth` to check for screensizes upon browser load. Hopefully this should fix the flakey js script issues with moving the **Register** tab.

![Mobile size](/assets/mobileSize.gif)

### 🎁 Additional Resources Used

* Fontawesome
* Google fonts (Source Sans Pro)
* Bootstrap for grid, carousel
* [Scotch panel](https://panels.scotch.io/) for mobile-only slide-in menu
* Badges from the google and apple app store
* [Box shadow generator](https://www.cssmatic.com/box-shadow)
* [Search box example](https://codepen.io/chriscoyier/pen/bYyvvz)